var express = require('express');

var Connection = require('./SqLiteConnection');
var dataService = require('./application/ModelData');
var service = new dataService();
var app = express(); // initialisation de l application
var bodyParser = require('body-parser'); 

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(bodyParser.json());//Lit l'élément Json dans l'url(s'il y en a)
app.use(bodyParser.urlencoded({ extended: true })); // Supporte les bodies encodés

// le chemin api fait reference a application
app.use('/', express.static('application'));

app.get('/data', (req, res) =>{
	const promise = service.getData();
	promise.then(function(value){
		res.end(JSON.stringify(value));
	});
});

var port = 1234;
var server = app.listen(port, function(){
	console.log("Demarrage sur localhost: "+port);
}); // le port qu'on bind
