  int buzzePin = 4;
  int sensorPin = 12;
  int compteur = 0;
  boolean val = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(buzzePin, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  val = digitalRead(sensorPin);
  Serial.print(val);
  if(val==LOW){
    compteur += 1 ;
    if(compteur==100){
       digitalWrite(buzzerPin, HIGH);
       delay(10000);
       compteur = 0;
    }
  }else{
    digitalWrite(buzzerPin, LOW);
  }
}
